<!DOCTYPE html>
<html lang="en">
    <?php    
    session_start();
    ?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
    <style>
        .frames {
            width: 500px;
            height: 500px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            margin-top: 30px;
        }

        .frames-margin {
            margin-top: 50px;
            margin-left: 25px;
        }

        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;

        }

        .display {
            width: 150px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display3 {
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 22px;
            background-color: aqua;
        }

        .checkbox2 {
            width: 30px;
            height: 22px;

        }

        .selectbox {
            width: 175px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }

        #signup {
            margin-left: 30%;

        }

        .customsignup {
            color: white;
            background-color: rgb(0, 162, 40);
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
        }
        
        
        
    </style>

</head>

<body>
    

    <div class="frames">
        <div class='frames-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>

            <table class='custom-table'>
                <tr>
                    <td class='display' style='background-color: #1bb857;'>Họ và tên</td>
                    <td>
                        <?php
                            echo $_SESSION["fullname"];
                        ?>
                    </td>
                    
                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Giới tính</td>
                    <td>
                        <?php
                            echo $_SESSION["gender"];
                        ?>
                    </td>

                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Phân khoa</td>
                    <td>
                        <?php
                            echo $_SESSION["faculty"];
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class='display' style='background-color: #1bb857;'>Ngày sinh</td>
                    <td>
                        <?php
                            echo $_SESSION["date"];
                        ?>
                    </td>
                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Địa chỉ</td>
                    <td>
                        <?php
                            echo $_SESSION["address"];
                        ?>
                    </td>
                </tr>

                <tr>
                    <td class="display" style="background-color: #1bb857;"> Hình ảnh</td>
                    <td>
                        <?php
                            echo "<img src = ". $_SESSION["hinhanh"] ." width = '100' height = '50'>";
                        ?>
                    </td>

                    
                </tr>

            </table>
             
            
        

            <div id="signup">
                <button class="customsignup">Xác nhận</button>
            </div>
        </div>
</body>

</html>